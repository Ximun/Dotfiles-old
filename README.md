# Fichiers de configuration de mon bureau i3

![Bureau](Images/bureau.jpg)

Voici les fichiers de configuration qui me servent lorsque je réinstalle un système qui intègre le gestionnaire de fenêtre i3.

## Copy symbol to clipboard
`echo -ne "\uf026" | xclip -selection clipboard`
