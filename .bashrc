#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\u \[\033[36m\]@ \h\[\033[00m\] \[\033[32m\]\w \[\033[00m\]$ \[\033[00m\]'

# aliases
alias vpn='sudo openvpn --config /home/simon/Documents/Aquilenet/vpn/aqn.ovpn --auth-nocache'
alias rm='rm -i'
alias ll='ls -al'
export http_proxy=''
export https_proxy=''
export ftp_proxy=''
export socks_proxy=''

export WORKON_HOME=~/.virenv
source /usr/bin/virtualenvwrapper.sh

# Accès aux binaires des gems ruby
PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
