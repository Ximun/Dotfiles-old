evaluate-commands %sh{

	#Bright Colors
	b_red="rgb:9d0006"
	b_green="rgb:046565"
	b_yellow="rgb:ef7333"
	b_blue="rgb:032531"
	b_purple="rgb:33004A"
	b_aqua="rgb:379662"
	b_orange="rgb:ccb277"
	b_gray="rgb:ddddd4"
	
	echo "
		face global LineNumbers rgb:333333
		face global LineNumberCursor rgb:046565
		face global Whitespace rgb:333333

		# Code highlighting
		# face global value     ${b_orange}
		# face global type      ${b_yellow}
		# face global variable  ${b_aqua}
		# face global module    ${b_yellow}
		# face global function  ${b_blue}
		# face global string    ${b_green}
		# face global keyword   ${b_purple}
		# face global operator  ${b_blue}
		# face global attribute ${b_orange}
		# face global comment   ${b_gray}+a
		# face global meta      ${b_red}
		# face global builtin   default+b
	"
}
